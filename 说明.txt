﻿本程序功能是实现企业微信对接微门禁开门，记录用户信息，记录钥匙信息，记录开门日志。

1.数据库脚本为wmj_qywx_user_key_log.sql
2.数据库连接参数文件为class/db.class.php
3.管理登录地址为http://域名/admin.php,用户名密码都是admin
4.登录后设置里面配置公众号信息和微门禁API接口信息，默认不加密；获取微门禁Api接口信息地址为https://www.wmj.com.cn/open/apilogin/index.html（注册登录后在API接入菜单下）

QQ微信：13886161
控制模块购买地址：https://weimenjin.taobao.com